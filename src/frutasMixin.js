export default {
    data() {
        return {
            fruta: '',
            frutas: ['banana','apple','orange']
        }
    },
    methods: {
        add() {
            this.frutas.push(this.fruta)                
            this.fruta = ''
        }
    },
}